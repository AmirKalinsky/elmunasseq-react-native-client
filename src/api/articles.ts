import axios from 'axios'
import { Article } from '../utils/models/Article'
import { useQuery } from '@tanstack/react-query'
import useRequest from '../common/hooks/useRequest'

const GET_ARTICLES_KEY = 'articles'
const GET_ARTICLE_KEY = 'article'

export const useGetArticles = () => {
  const request = useRequest()

  const fetchArticles = () => request<Article[]>('GET', `/${GET_ARTICLES_KEY}`)

  const { data, isError, isLoading } = useQuery<Article[], Error>({
    queryKey: [GET_ARTICLES_KEY],
    queryFn: fetchArticles,
  })

  return { articles: data, isError, isLoading }
}

const useGetArticleById = (id: string) => {
  const request = useRequest()

  const fetchArticle = (id: string) => request<Article>('GET', `/${GET_ARTICLE_KEY}/${id}`)

  const { data, isError, isLoading } = useQuery<Article, Error>({
    queryKey: [GET_ARTICLE_KEY, id],
    queryFn: () => fetchArticle(id),
  })

  return { articles: data, isError, isLoading }
}

export default { useGetArticles, useGetArticleById }
