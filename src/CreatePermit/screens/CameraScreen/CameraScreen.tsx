import React, { FC, memo, useCallback, useEffect, useRef, useState } from 'react'
import RNFS from 'react-native-fs'
import { Camera, CameraPermissionStatus, useCameraDevices } from 'react-native-vision-camera'
import { Button, StyleSheet, View } from 'react-native'
import { useIsFocused } from '@react-navigation/native'
import DocumentPicker from 'react-native-document-picker'
import Logger from '../../../utils/services/logger'
import { useBottomSheet } from '../../../common/providers/BottomSheet/BottomSheetProvider'
import { useGetArticles } from '../../../api/articles'

const CameraScreen: FC = () => {
  const [cameraPermission, setCameraPermission] = useState<CameraPermissionStatus | null>(null)

  const getCurrentPermissions = async () => {
    const cameraPermission = await Camera.getCameraPermissionStatus()
    if (cameraPermission !== 'authorized') {
      const newCameraPermission = await Camera.requestCameraPermission()
      setCameraPermission(newCameraPermission)
    } else {
      setCameraPermission(cameraPermission)
    }
  }

  const isPermissionGranted = cameraPermission === 'authorized'

  useEffect(() => {
    if (!cameraPermission) {
      getCurrentPermissions()
    }
  }, [cameraPermission])

  return isPermissionGranted ? <CameraView /> : <View />
}
const CameraView: FC = () => {
  const devices = useCameraDevices()
  const isFocused = useIsFocused()
  const camera = useRef<Camera>(null)
  const device = devices.back

  const takePhoto = useCallback(async () => {
    const photo = await camera.current?.takePhoto()
    if (photo) {
      const { path } = photo
      const base64Image = await RNFS.readFile(path, 'base64')
      Logger.debug(base64Image)
    }
  }, [camera.current])

  const openFileSystem = useCallback(async () => {
    const { uri } = await DocumentPicker.pickSingle()
    const base64Doc = await RNFS.readFile(uri, 'base64')
    Logger.debug(base64Doc)
  }, [camera.current])

  if (device == null) return <View />

  return (
    <View style={StyleSheet.absoluteFill}>
      <Camera
        ref={camera}
        style={StyleSheet.absoluteFill}
        device={device}
        isActive={isFocused}
        photo={true}
      />
      <Button title="Take Photo" onPress={takePhoto} />
      <Button title="Upload Doc" onPress={openFileSystem} />
    </View>
  )
}

export default memo(CameraScreen)
