import React, { FC, ReactNode } from 'react'
import { View } from 'react-native'
import styles from './styles'

type ScreenProps = {
  children: ReactNode
}
const Screen: FC<ScreenProps> = ({ children }) => <View style={styles.screen}>{children}</View>

export default Screen
