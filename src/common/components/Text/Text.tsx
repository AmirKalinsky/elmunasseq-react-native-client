import { FC } from 'react'
import { Text as RNText, TextProps as RNTextProps } from 'react-native'
import { useConfig } from '../../providers/Config/ConfigProvider'

type TextProps = Omit<RNTextProps, 'children'> & {
  hebText: string
  arabText: string
}

const Text: FC<TextProps> = ({ hebText, arabText, ...props }) => {
  const { lang } = useConfig()
  const text = lang === 'Hebrew' ? hebText : arabText

  return <RNText {...props}>{text}</RNText>
}

export default Text
