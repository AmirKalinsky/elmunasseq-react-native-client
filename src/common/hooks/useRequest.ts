import { useConfig } from '../providers/Config/ConfigProvider'
import { useCallback } from 'react'
import axios, { AxiosRequestConfig, AxiosRequestHeaders, AxiosResponse, Method } from 'axios'
import Logger from '../../utils/services/logger'

const headers: Partial<AxiosRequestHeaders> = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
}

const useRequest = () => {
  const { user } = useConfig()

  const getReqConfig = useCallback(
    <DataType = unknown>(method: Method, url: string, data?: DataType) => {
      if (!user) {
        throw new Error('No user data')
      }
      const requestObject: AxiosRequestConfig<DataType> = {
        method,
        url,
        headers: { ...headers, Authorization: `Bearer ${user}` } as AxiosRequestHeaders,
      }
      if (data) {
        const isMethodWithBody = method === 'POST' || method === 'PUT'
        isMethodWithBody ? (requestObject['data'] = data) : (requestObject['params'] = data)
      }

      return requestObject
    },
    [user]
  )

  const request = useCallback(
    async <ResType, DataType = unknown>(
      method: Method,
      url: string,
      data?: DataType
    ): Promise<ResType> => {
      try {
        const reqConfig = getReqConfig<DataType>(method, url, data)
        const res: AxiosResponse<ResType> = await axios.request<ResType>(reqConfig)

        return res.data
      } catch (err: unknown) {
        if (axios.isAxiosError(err)) {
          throw {
            name: err.name,
            message: err.message,
            status: err.response?.status || 0,
          }
        } else {
          Logger.error('Request failed with error: ' + JSON.stringify(err))
          throw err
        }
      }
    },
    [getReqConfig]
  )

  return request
}

export default useRequest
