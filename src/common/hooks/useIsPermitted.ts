import { ComponentType } from 'react'

const COMPONENTS_PERMISSION: { [key: string]: boolean } = {
  ['Home']: true,
}

const useIsPermitted = (): boolean => {
  const callerComponentName = (useIsPermitted.caller as ComponentType<unknown>).displayName
  if (callerComponentName === undefined) {
    return true
  }

  const isComponentEnforcedWithPermissions = callerComponentName in COMPONENTS_PERMISSION

  if (!isComponentEnforcedWithPermissions) {
    return true
  }

  return COMPONENTS_PERMISSION[callerComponentName]
}

export default useIsPermitted
