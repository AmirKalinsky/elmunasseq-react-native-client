import React, { FC, memo, ReactElement, useCallback } from 'react'
import useRequest from '../../hooks/useRequest'
import Logger from '../../../utils/services/logger'
import ErrorBoundary from 'react-native-error-boundary'

type ErrorBoundaryProps = {
  children: ReactElement
}

const ErrorBoundaryProvider: FC<ErrorBoundaryProps> = ({ children }) => {
  const request = useRequest()

  const logError = useCallback(
    ({ name, message }: Error, stackTrace: string) => {
      Logger.error({ error: { name, message }, stackTrace })
    },
    [request]
  )

  return <ErrorBoundary onError={logError}>{children}</ErrorBoundary>
}
export default memo(ErrorBoundaryProvider)
