import React, {
  createContext,
  FC,
  ReactNode,
  useCallback,
  useContext,
  useRef,
  useState,
} from 'react'
import { Modalize, ModalizeProps } from 'react-native-modalize'
import { GestureHandlerRootView } from 'react-native-gesture-handler'
import { StyleSheet } from 'react-native'

type BottomSheetState = {
  openBottomSheet: (component: ReactNode, props?: ModalizeProps) => void
  closeBottomSheet: () => void
}

const BottomSheetContext = createContext<BottomSheetState | null>(null)

type BottomSheetProviderProps = {
  children: ReactNode
}

const BottomSheetProvider: FC<BottomSheetProviderProps> = ({ children }) => {
  const [component, setComponent] = useState<ReactNode | null>(null)
  const [props, setProps] = useState<ModalizeProps | null>(null)
  const bottomSheetRef = useRef<Modalize | null>(null)

  const openBottomSheet = useCallback(
    (component: ReactNode, props?: ModalizeProps) => {
      setComponent(component)
      props && setProps(props)
      bottomSheetRef.current?.open()
    },
    [bottomSheetRef.current]
  )

  const closeBottomSheet = useCallback(() => {
    bottomSheetRef.current?.close()
  }, [bottomSheetRef.current])

  const onBottomSheetClosed = useCallback(() => {
    setComponent(null)
    setProps(null)
  }, [])

  return (
    <GestureHandlerRootView style={StyleSheet.absoluteFill}>
      <BottomSheetContext.Provider value={{ openBottomSheet, closeBottomSheet }}>
        {children}
        <Modalize
          ref={bottomSheetRef}
          adjustToContentHeight
          handlePosition="inside"
          disableScrollIfPossible={false}
          onClosed={onBottomSheetClosed}
          {...props}
        >
          {component}
        </Modalize>
      </BottomSheetContext.Provider>
    </GestureHandlerRootView>
  )
}

const useBottomSheet = (): BottomSheetState => {
  const context = useContext<BottomSheetState | null>(BottomSheetContext)

  if (!context) {
    throw new Error('BottomSheet must be used within a BottomSheetProvider')
  }

  return context
}

export { BottomSheetProvider, useBottomSheet }
