import {
  Context,
  createContext,
  Dispatch,
  FC,
  ReactNode,
  SetStateAction,
  useContext,
  useEffect,
  useState,
} from 'react'
import DeviceStorage, { StorageKey } from '../../../utils/services/device-storage'
import { View } from 'react-native'

const DEVICE_STORAGE_CONFIG_KEY: StorageKey = '@config'

export type ConfigState = {
  user: string | null
  setUser: Dispatch<SetStateAction<string | null>>
  lang: 'Arabic' | 'Hebrew'
  setLang: Dispatch<SetStateAction<'Arabic' | 'Hebrew'>>
}

type StoredConfigState = Pick<ConfigState, 'user' | 'lang'>

const defaultContextState: Omit<ConfigState, 'setLang' | 'setUser'> = {
  user: null,
  lang: 'Hebrew',
}

export const ConfigContext: Context<ConfigState> = createContext<ConfigState>(
  defaultContextState as ConfigState
)

//TODO Change to the relevant types
export const ConfigProvider: FC<{ children: ReactNode }> = ({ children }) => {
  const [user, setUser] = useState<string | null>(null)
  const [lang, setLang] = useState<'Arabic' | 'Hebrew'>('Hebrew')
  const [isLoading, setIsLoading] = useState(false)

  const loadLocalConfig = async () => {
    setIsLoading(true)
    const config = await DeviceStorage.get<StoredConfigState>(DEVICE_STORAGE_CONFIG_KEY)
    if (config) {
      const { user, lang } = config
      setUser(user)
      setLang(lang)
    }
    setIsLoading(false)
  }

  const saveToLocalStorage = async () => {
    const currentConfig = { user, lang }
    const isConfigChanged = JSON.stringify(currentConfig) !== JSON.stringify(defaultContextState)
    if (isConfigChanged) {
      await DeviceStorage.set<StoredConfigState>(DEVICE_STORAGE_CONFIG_KEY, { user, lang })
    }
  }

  //TODO you can add a refresh interval to the dependency array
  useEffect(() => {
    loadLocalConfig()
  }, [])

  useEffect(() => {
    saveToLocalStorage()
  }, [user, lang])

  //TODO Implement loading screen
  return (
    <ConfigContext.Provider value={{ user, setUser, lang, setLang }}>
      {isLoading ? <View /> : children}
    </ConfigContext.Provider>
  )
}
export const useConfig = () => useContext<ConfigState>(ConfigContext)
