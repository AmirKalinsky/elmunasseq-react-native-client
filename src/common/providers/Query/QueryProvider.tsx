import React, { FC, ReactNode, useEffect } from 'react'
import { AppState, AppStateStatus, Platform } from 'react-native'
import NetInfo from '@react-native-community/netinfo'
import {
  focusManager,
  onlineManager,
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query'

onlineManager.setEventListener((setOnline) =>
  NetInfo.addEventListener((state) => {
    setOnline(!!state.isConnected)
  })
)

const onAppStateChange = (status: AppStateStatus) => {
  if (Platform.OS !== 'web') {
    // eslint-disable-next-line no-undef
    focusManager.setFocused(status === 'active')
  }
}

const queryClient = new QueryClient()

const QueryProvider: FC<{ children: ReactNode }> = ({ children }) => {
  useEffect(() => {
    const subscription = AppState.addEventListener('change', onAppStateChange)

    return () => subscription.remove()
  }, [])

  return <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
}

export default QueryProvider
