import React, { FC, memo } from 'react'
import { View } from 'react-native'

const Profile: FC = () => <View />

export default memo(Profile)
