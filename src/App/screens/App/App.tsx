import React, { FC } from 'react'
import 'react-native-reanimated'
import AppNavigator from '../../navigator/AppNavigator'
import { ConfigProvider } from '../../../common/providers/Config/ConfigProvider'
import { BottomSheetProvider } from '../../../common/providers/BottomSheet/BottomSheetProvider'
import QueryProvider from '../../../common/providers/Query/QueryProvider'
import ErrorBoundaryProvider from '../../../common/providers/ErrorBoundaryProvider/ErrorBoundaryProvider'

const App: FC = () => (
  <ErrorBoundaryProvider>
    <ConfigProvider>
      <QueryProvider>
        <BottomSheetProvider>
          <AppNavigator />
        </BottomSheetProvider>
      </QueryProvider>
    </ConfigProvider>
  </ErrorBoundaryProvider>
)

export default App
