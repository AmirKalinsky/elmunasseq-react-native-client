import React, { FC, memo } from 'react'
import { View } from 'react-native'

const Landing: FC = () => <View />

export default memo(Landing)
