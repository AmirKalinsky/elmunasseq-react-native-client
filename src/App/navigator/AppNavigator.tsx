import React, { FC, memo } from 'react'
import {
  createNativeStackNavigator,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack'
import { NavigationContainer } from '@react-navigation/native'

import Landing from '../screens/Landing/Landing'
import AuthNavigator from '../../Auth/navigator/AuthNavigator'
import BottomTabsNavigator from './BottomTabNavigator'
import { useConfig } from '../../common/providers/Config/ConfigProvider'

type AppNavigatorParamList = {
  BottomTabs: undefined
  Landing: undefined
  Auth: undefined
}

const AppStackNavigator = createNativeStackNavigator<AppNavigatorParamList>()

const appNavigatorConfig: NativeStackNavigationOptions = { headerShown: false }

const AppNavigator: FC = () => {
  const { user } = useConfig()

  return (
    <NavigationContainer>
      <AppStackNavigator.Navigator
        screenOptions={appNavigatorConfig}
        initialRouteName={user ? 'BottomTabs' : 'Auth'}
      >
        <AppStackNavigator.Screen name="BottomTabs" component={BottomTabsNavigator} />
        <AppStackNavigator.Screen name="Landing" component={Landing} />
        <AppStackNavigator.Screen name="Auth" component={AuthNavigator} />
      </AppStackNavigator.Navigator>
    </NavigationContainer>
  )
}
export default memo(AppNavigator)
