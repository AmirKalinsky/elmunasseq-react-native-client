import React, { FC, memo } from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import MainNavigator from '../../Main/navigator/MainNavigator'
import Profile from '../screens/Profile/Profile'

type BottomToolbarNavigatorParamList = {
  Main: undefined
  Profile: undefined
}

const BottomToolbarNavigator = createBottomTabNavigator<BottomToolbarNavigatorParamList>()

const BottomTabsNavigator: FC = () => (
  <BottomToolbarNavigator.Navigator>
    <BottomToolbarNavigator.Screen name="Main" component={MainNavigator} />
    <BottomToolbarNavigator.Screen name="Profile" component={Profile} />
  </BottomToolbarNavigator.Navigator>
)

export default memo(BottomTabsNavigator)
