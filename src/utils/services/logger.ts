import { configLoggerType, logger } from 'react-native-logs'

const config: configLoggerType = {
  levels: {
    custom: 0,
    debug: 1,
    info: 2,
    warn: 3,
    error: 4,
  },
  transportOptions: {
    colors: {
      info: 'blueBright',
      warn: 'yellowBright',
      error: 'redBright',
      debug: 'white',
    },
  },
}

class Logger {
  private logger
  constructor() {
    this.logger = logger.createLogger(config)
  }
  debug(...args: unknown[]) {
    this.logger.debug(...args)
  }
  info(...args: unknown[]) {
    this.logger.info(...args)
  }
  warn(...args: unknown[]) {
    this.logger.warn(...args)
  }
  error(...args: unknown[]) {
    this.logger.error(...args)
  }
}

export default new Logger()
