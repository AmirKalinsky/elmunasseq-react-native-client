import AsyncStorage from '@react-native-async-storage/async-storage'
import Logger from './logger'

export type StorageKey = `@${Lowercase<string>}`

class DeviceStorage {
  async get<T>(key: StorageKey): Promise<T | undefined> {
    try {
      const serializedValue = await AsyncStorage.getItem(key)
      if (serializedValue) {
        return JSON.parse(serializedValue) as T
      } else {
        Logger.warn(`Storage GET operation with key: ${key} resulted with no value`)
      }
    } catch (error) {
      Logger.error(
        `Storage GET operation with key: ${key} failed with the following error: ${error}`
      )
    }
  }
  async set<T>(key: StorageKey, value: T) {
    try {
      const serializedValue = this.serializeValue<T>(value)
      await AsyncStorage.setItem(key, serializedValue)
      Logger.info(`Successfully saved value: ${serializedValue} to ${key} key`)
    } catch (error) {
      Logger.error(
        `Storage SET operation with key: ${key} failed with the following error: ${error}`
      )
    }
  }
  async delete(key: StorageKey) {
    try {
      await AsyncStorage.removeItem(key)
      Logger.info(`Successfully deleted ${key}.`)
    } catch (error) {
      Logger.error(
        `Storage DELETE operation with key: ${key} failed with the following error: ${error}`
      )
    }
  }

  async clear() {
    try {
      await AsyncStorage.clear()
      Logger.info(`Successfully flushed device storage.`)
    } catch (error) {
      Logger.error(`Storage FLUSH operation failed with the following error: ${error}`)
    }
  }
  private serializeValue<T>(value: T) {
    return JSON.stringify(value)
  }
}

export default new DeviceStorage()
