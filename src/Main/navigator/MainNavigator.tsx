import React, { FC, memo } from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import Home from '../screens/Home/Home'
import MyRequests from '../screens/MyRequests/MyRequests'
import NewsArticle from '../screens/NewsArticle/NewsArticle'
import PreventionStatus from '../screens/PreventionStatus/PreventionStatus'

type MainNavigatorParamList = {
  Home: undefined
  PermitRequest: undefined
  MyRequests: undefined
  NewsArticle: undefined
  PreventionStatus: undefined
}

const MainStackNavigator = createNativeStackNavigator<MainNavigatorParamList>()

const MainNavigator: FC = () => (
  <MainStackNavigator.Navigator initialRouteName="Home">
    <MainStackNavigator.Screen name="Home" component={Home} />
    <MainStackNavigator.Screen name="MyRequests" component={MyRequests} />
    <MainStackNavigator.Screen name="NewsArticle" component={NewsArticle} />
    <MainStackNavigator.Screen name="PreventionStatus" component={PreventionStatus} />
  </MainStackNavigator.Navigator>
)

export default memo(MainNavigator)
