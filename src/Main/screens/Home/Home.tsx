import React, { FC } from 'react'
import { Text } from 'react-native'
import Screen from '../../../common/components/Screen/Screen'

const Home: FC = () => (
  <Screen>
    <Text>HOME</Text>
  </Screen>
)

export default Home
