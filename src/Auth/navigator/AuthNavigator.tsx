import React, { FC, memo } from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import Login from '../screens/Login/Login'
import Register from '../screens/Register/Register'

type AuthNavigatorParamList = {
  Register: undefined
  Login: undefined
}

const AuthStackNavigator = createNativeStackNavigator<AuthNavigatorParamList>()

const AuthNavigator: FC = () => (
  <AuthStackNavigator.Navigator initialRouteName="Login">
    <AuthStackNavigator.Screen name="Login" component={Login} />
    <AuthStackNavigator.Screen name="Register" component={Register} />
  </AuthStackNavigator.Navigator>
)

export default memo(AuthNavigator)
